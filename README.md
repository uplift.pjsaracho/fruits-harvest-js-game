# Fruits Harvest (Javascript Game)

> Fruits Harvest is a javascript-based browser game.  
> Just open the index.html file in your browser.  
> You have 15 seconds to catch as many fruits as you can.  

## Contact Information

Feel free to reach out to and connect with the instructor for any inquiries or problems with the sample project.

**PeeJay Saracho**  
0915 980 1701  
uplift.pjsaracho@gmail.com  
