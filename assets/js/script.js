let screen = document.querySelector("#screen");
let basket = document.querySelector("#basket");
let score = document.querySelector("#score div");
let play = document.querySelector("#play");

screen.addEventListener("mousemove", function() {
	basket.style.cursor = "none";
	basket.style.top = event.clientY-basket.clientHeight/3 +"px";
	basket.style.left = event.clientX +"px";
});

function item1Fall() {
	var tempDiv = document.createElement('div');
	tempDiv.innerHTML = `<div class="item1"></div>`;
	let item1 = tempDiv.firstChild;
	screen.appendChild(item1);
	item1.addEventListener("mouseover", function() {
		clearInterval(id);
		item1.parentNode.removeChild(item1);
		score.innerHTML = score.innerHTML*1 + 10;
	});
	let item1x = Math.floor( Math.random() * (screen.clientWidth-item1.clientWidth) );
	item1.style.left = item1x+"px";
	var pos = 0;
	var id = setInterval(frame, 1);
	function frame() {
		if( pos == (screen.clientHeight) ) {
			clearInterval(id);
			item1.parentNode.removeChild(item1);
		} else {
			pos+= (Math.floor(Math.random()*150)+250)/100;
			item1.style.top = pos + 'px';
		}
	}
}

function item2Fall() {
	var tempDiv = document.createElement('div');
	tempDiv.innerHTML = `<div class="item2"></div>`;
	let item1 = tempDiv.firstChild;
	screen.appendChild(item1);
	item1.addEventListener("mouseover", function() {
		clearInterval(id);
		item1.parentNode.removeChild(item1);
		score.innerHTML = score.innerHTML*1 + 1;
	});
	let item1x = Math.floor( Math.random() * (screen.clientWidth-item1.clientWidth) );
	item1.style.left = item1x+"px";
	var pos = 0;
	var id = setInterval(frame, 1);
	function frame() {
		if( pos == (screen.clientHeight) ) {
			clearInterval(id);
			item1.parentNode.removeChild(item1);
		} else {
			pos+= (Math.floor(Math.random()*250)+100)/100;
			item1.style.top = pos + 'px';
		}
	}
}

function item3Fall() {
	var tempDiv = document.createElement('div');
	tempDiv.innerHTML = `<div class="item3"></div>`;
	let item1 = tempDiv.firstChild;
	screen.appendChild(item1);
	item1.addEventListener("mouseover", function() {
		clearInterval(id);
		item1.parentNode.removeChild(item1);
		score.innerHTML = score.innerHTML*1 + 3;
	});
	let item1x = Math.floor( Math.random() * (screen.clientWidth-item1.clientWidth) );
	item1.style.left = item1x+"px";
	var pos = 0;
	var id = setInterval(frame, 1);
	function frame() {
		if( pos == (screen.clientHeight) ) {
			clearInterval(id);
			item1.parentNode.removeChild(item1);
		} else {
			pos+=1.5;
			item1.style.top = pos + 'px';
		}
	}
}

play.addEventListener("click", function() {
	let secs = 15;
	function countdown() {
		let counter = setInterval(function() {
			secs--;
			document.querySelector("#timer").innerHTML = `Time remaining: <div>${secs}</div>`;
			if(secs==0) {
				clearInterval(counter);
				document.querySelector(".modal").style.display = "block";
				document.querySelector(".modal-content").innerHTML = "Game Over. Your Score is: <br>"+
				`<div class="finalScore">${score.innerHTML}</div>`;
				document.querySelector(".modal-content").style.textAlign = "center";
				document.querySelector(".modal-content").style.fontSize = "36px";
				document.querySelector(".modal-content").style.height = "50%";
				document.querySelector(".modal-content").style.marginTop = "15%";
			}
		}, 1000)
	}
	countdown();
	document.querySelector(".modal").style.display = "none";
	let i1fall = setInterval( function() {
		if(secs<=0) clearInterval(i1fall);
		item1Fall();
	}, 3000);

	let i2fall = setInterval( function() {
		if(secs<=0) clearInterval(i2fall);
		item2Fall();
	}, 1000);

	let i3fall = setInterval( function() {
		if(secs<=0) clearInterval(i3fall);
		item3Fall();
	}, 2000);
});
